const express = require('express')
const router = express.Router()
const admins = require("../data/admins.json")


router.get("/login",(req,res) =>{
    res.render("pages/login",{error : null})
})

router.post("/login",(req,res) =>{
    
    const username = req.body.username
    const password = req.body.password

    const findUser = admins.find(
        (admin) => admin.username === username && admin.password === password
    )

    if(!findUser){  
        res.render("pages/login",{error:"invalid login"})
        return
    }
     
    res.redirect("/activities")
})


module.exports = router